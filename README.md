Demo Kubernetes Platform
========================

This repository accompanies a series of blog posts on
["Building a Kubernetes Platform for Fun and Profit"](http://https://medium.com/building-a-kubernetes-platform-for-fun-and-profit).

Please see the material [here](https://medium.com/building-a-kubernetes-platform-for-fun-and-profit/building-a-kubernetes-platform-for-fun-and-profit-part-one-fc7f10fd1a5e)
to get started.
