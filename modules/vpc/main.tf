locals {
  tags = {
    Environment = var.cluster_name
    Terraform   = true
  }
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "random_shuffle" "az" {
  input        = data.aws_availability_zones.available.names
  result_count = 3
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.33"

  name = "eks-${var.cluster_name}"
  cidr = "192.168.0.0/16"

  azs = [
    random_shuffle.az.result[0],
    random_shuffle.az.result[1],
    random_shuffle.az.result[2]
  ]

  private_subnets = [
    "192.168.1.0/24",
    "192.168.2.0/24",
    "192.168.3.0/24"
  ]

  public_subnets = [
    "192.168.101.0/24",
    "192.168.102.0/24",
    "192.168.103.0/24"
  ]

  enable_nat_gateway = true

  tags = local.tags
}
