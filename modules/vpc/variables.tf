variable "cluster_name" {
  description = "name for the cluster to use in resource labels"
  type        = string
}
