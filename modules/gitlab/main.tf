locals {
  tags = {
    Environment = var.runner_name
    Terraform   = true
  }
}

data "aws_region" "current" {}

resource "random_shuffle" "spawner_subnet_id" {
  input        = var.vpc_subnet_ids
  result_count = 1
}

data "aws_subnet" "spawner_subnet" {
  id = random_shuffle.spawner_subnet_id.result[0]
}

module "runner" {
  source = "git::https://github.com/npalm/terraform-aws-gitlab-runner.git?ref=4.15.0"

  aws_region  = data.aws_region.current.name
  aws_zone    = data.aws_subnet.spawner_subnet.availability_zone_id
  environment = var.runner_name

  cloudwatch_logging_retention_in_days = 1
  enable_forced_updates                = true
  instance_type                        = var.instance_type
  ssh_key_pair                         = var.ssh_keypair

  runners_use_private_address = false
  enable_eip                  = false

  vpc_id                   = var.vpc_id
  subnet_ids_gitlab_runner = var.vpc_subnet_ids
  subnet_id_runners        = data.aws_subnet.spawner_subnet.id

  runners_executor   = "docker"
  runners_name       = var.runner_name
  runners_gitlab_url = var.gitlab_url

  gitlab_runner_registration_config = {
    locked_to_project  = true
    description        = var.runner_name
    maximum_timeout    = 3600
    registration_token = var.registration_token
    run_untagged       = true
    tag_list           = "cluster-gitlab-runner"
  }

  tags = local.tags
}

data "aws_caller_identity" "current" {}

resource "aws_iam_role_policy" "terraform-permissions" {
  name = "terraform-s3-dynamo-permissions"
  role = module.runner.runner_agent_role_name
  policy = templatefile("${path.module}/policies/terraform-permissions.json", {
    current_account_id          = data.aws_caller_identity.current.account_id
    terraform_state_bucket_name = var.terraform_state_bucket_name,
    terraform_lock_table_name   = var.terraform_lock_table_name
  })
}

resource "aws_iam_role_policy" "eks-permissions" {
  name   = "orchestrate-eks-permissions"
  role   = module.runner.runner_agent_role_name
  policy = file("${path.module}/policies/eks-permissions.json")
}

resource "aws_iam_role_policy" "assume-cluster-admin-role" {
  name = "assume-cluster-admin-role"
  role = module.runner.runner_agent_role_name
  policy = templatefile("${path.module}/policies/assume-cluster-admin-role.json", {
    cluster_admin_role_arn = var.cluster_admin_role_arn,
  })
}
