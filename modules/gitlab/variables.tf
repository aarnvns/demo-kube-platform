variable "cluster_admin_role_arn" {
  description = "ARN of role that has cluster admin permission"
  type        = string
}

variable "gitlab_url" {
  description = "URL of the gitlab instance to connect to."
  type        = string
  default     = "https://gitlab.com"
}

variable "instance_type" {
  description = "EC2 instance type for runner"
  type        = string
  default     = "t3.nano"
}

variable "runner_name" {
  description = "Name for this gitlab-runner used in labels"
  type        = string
}

variable "registration_token" {
  description = "Token to authenticate this runner with a Gitlab project"
  type        = string
}

variable "ssh_keypair" {
  description = "Optional EC2 keypair to attach to runner (for debugging only)"
  type        = string
  default     = ""
}

variable "terraform_lock_table_name" {
  description = "Name of the DynamoDB lock table used by Terraform"
  type        = string
}

variable "terraform_state_bucket_name" {
  description = "Name of the S3 state bucket used by Terraform"
  type        = string
}

variable "vpc_id" {
  description = "ID of VPC to use for EKS"
  type        = string
}

variable "vpc_subnet_ids" {
  description = "IDs of VPC subnets to use for EKS"
  type        = list(string)
}
