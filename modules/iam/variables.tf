variable "iam_cluster_admin_role" {
  description = "Special role that can be assumed for Kubernetes cluster admin permission"
  type        = string
}
