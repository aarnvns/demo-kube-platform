# This is a special role which provides cluster-admin permission in the context of RBAC.
# This is not intended to be used for granting IAM permissions.
data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

resource "aws_iam_role" "cluster_admin_role" {
  name               = var.iam_cluster_admin_role
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}
