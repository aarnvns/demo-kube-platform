locals {
  cluster_admin_role_arns = concat([{ role_arn : var.cluster_admin_role_arn }], var.cluster_admin_role_arns)

  tags = {
    Environment = var.cluster_name
    Terraform   = true
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "11.1.0"

  cluster_name    = var.cluster_name
  cluster_version = "1.15"

  vpc_id  = var.vpc_id
  subnets = var.vpc_subnet_ids

  cluster_endpoint_public_access_cidrs = var.cluster_whitelisted_cidrs

  map_roles = [for m in local.cluster_admin_role_arns : {
    rolearn  = m.role_arn
    username = m.role_arn
    groups   = ["system:masters"]
  }]
  map_users = [for m in var.cluster_admin_user_arns : {
    userarn  = m.user_arn
    username = m.user_arn
    groups   = ["system:masters"]
  }]

  worker_groups = [
    {
      instance_type = var.workers_instance_type
      asg_max_size  = var.workers_asg_max
    }
  ]

  tags = local.tags
}

resource "local_file" "kubeconfig" {
  content  = module.eks.kubeconfig
  filename = var.kubeconfig_path
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token

  # avoid loading a config file, or else it might load one accidentally referenced in $KUBECONFIG
  # and change the wrong cluster!
  load_config_file = false
}
