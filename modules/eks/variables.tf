variable "cluster_admin_role_arn" {
  description = "Special IAM role having the right to assume cluster admin privilege"
  type        = string
}

variable "cluster_admin_role_arns" {
  description = "List of IAM role ARNs to grant cluster admin access"
  type = list(object({
    role_arn = string
  }))
  default = []
}

variable "cluster_admin_user_arns" {
  description = "List of IAM user ARNs to grant cluster admin access"
  type = list(object({
    user_arn = string
  }))
  default = []
}

variable "cluster_name" {
  description = "name for the cluster to use in resource labels"
  type        = string
}

variable "cluster_whitelisted_cidrs" {
  description = "IP addresses that are allowed to connect to the Kubernetes cluster"
  type        = list(string)
}

variable "kubeconfig_path" {
  description = "Path to write kubeconfig artifact"
  type        = string
}

variable "vpc_id" {
  description = "ID of VPC to use for EKS"
  type        = string
}

variable "vpc_subnet_ids" {
  description = "IDs of VPC subnets to use for EKS"
  type        = list(string)
}

variable "workers_asg_max" {
  description = "limit for the size of the cluster workers ASG"
  type        = number
}

variable "workers_instance_type" {
  description = "EC2 instance type for worker nodes"
  type        = string
}
