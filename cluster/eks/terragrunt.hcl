terraform {
  source = "../../modules/eks"
}

locals {
  cluster_vars = read_terragrunt_config(find_in_parent_folders("variables.hcl"))

  cluster_admin_role_arns = [for r in local.cluster_vars.locals.kube_iam_trusted_roles : {
    role_arn = r
  }]
  cluster_admin_user_arns = [for u in local.cluster_vars.locals.kube_iam_trusted_users : {
    user_arn = u
  }]
  kubeconfig_path = "${get_parent_terragrunt_dir()}/artifacts/kubeconfig"
}

include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../iam", "../vpc"]
}

dependency "iam" {
  config_path = "../iam"

  mock_outputs_allowed_terraform_commands = ["validate", "plan"]
  mock_outputs = {
    cluster_admin_role_arn = "fake-iam-role-arn"
  }
}

dependency "vpc" {
  config_path = "../vpc"

  mock_outputs_allowed_terraform_commands = ["validate", "plan"]
  mock_outputs = {
    vpc_id                 = "fake-vpc-id"
    vpc_private_subnet_ids = ["fake-subnet-id1", "fake-subnet-id2"]
  }
}

inputs = {
  cluster_admin_role_arn    = dependency.iam.outputs.cluster_admin_role_arn
  cluster_admin_role_arns   = local.cluster_admin_role_arns
  cluster_admin_user_arns   = local.cluster_admin_user_arns
  cluster_name              = local.cluster_vars.locals.cluster_name
  cluster_whitelisted_cidrs = local.cluster_vars.locals.kube_whitelist_ip_cidrs
  kubeconfig_path           = local.kubeconfig_path
  vpc_id                    = dependency.vpc.outputs.vpc_id
  vpc_subnet_ids            = dependency.vpc.outputs.vpc_private_subnet_ids
  workers_asg_max           = local.cluster_vars.locals.kube_workers_asg_max
  workers_instance_type     = local.cluster_vars.locals.kube_workers_instance_type
}
