# defines global variables necessary for each cluster under the "clusters" directory
locals {
  # general settings
  aws_region   = "us-east-2"
  cluster_name = "mycluster-eks"

  # IAM settings
  iam_cluster_admin_role = "@ClusterAdmin" # required: a special role that will be created for assuming cluster admin privilege

  # terraform settings
  terraform_lock_table_name   = "terraform-lock-table"
  terraform_state_bucket_name = "${get_aws_account_id()}-terraform-state"

  # kubernetes settings
  kube_iam_trusted_roles     = []            # optional: grand cluster admin to IAM roles
  kube_iam_trusted_users     = []            # optional: grant cluster admin to IAM users
  kube_whitelist_ip_cidrs    = ["0.0.0.0/0"] # optional: restrict access to cluster API
  kube_workers_asg_max       = 1
  kube_workers_instance_type = "m5.large"
}
