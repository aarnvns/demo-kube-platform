terraform {
  source = "../../modules/iam"
}

locals {
  cluster_vars = read_terragrunt_config(find_in_parent_folders("variables.hcl"))
}

include {
  path = find_in_parent_folders()
}

inputs = {
  iam_cluster_admin_role = local.cluster_vars.locals.iam_cluster_admin_role
}
