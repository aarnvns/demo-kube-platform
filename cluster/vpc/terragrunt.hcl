terraform {
  source = "../../modules/vpc"
}

locals {
  cluster_vars = read_terragrunt_config(find_in_parent_folders("variables.hcl"))
}

include {
  path = find_in_parent_folders()
}

inputs = {
  cluster_name = local.cluster_vars.locals.cluster_name
}
