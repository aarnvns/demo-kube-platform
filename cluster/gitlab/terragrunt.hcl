terraform {
  source = "../../modules/gitlab"
}

locals {
  cluster_vars                = read_terragrunt_config(find_in_parent_folders("variables.hcl"))
  cluster_name                = local.cluster_vars.locals.cluster_name
  terraform_state_bucket_name = local.cluster_vars.locals.terraform_state_bucket_name
  terraform_lock_table_name   = local.cluster_vars.locals.terraform_lock_table_name
  use_private_subnets         = true
}

include {
  path = find_in_parent_folders()
}

dependencies {
  paths = ["../iam", "../vpc"]
}

dependency "iam" {
  config_path = "../iam"

  mock_outputs_allowed_terraform_commands = ["validate", "plan"]
  mock_outputs = {
    cluster_admin_role_arn = "fake-iam-role-arn"
  }
}

dependency "vpc" {
  config_path = "../vpc"

  mock_outputs_allowed_terraform_commands = ["validate", "plan"]
  mock_outputs = {
    vpc_id                 = "fake-vpc-id"
    vpc_private_subnet_ids = ["fake-subnet-id1", "fake-subnet-id2"]
  }
}

inputs = {
  cluster_admin_role_arn      = dependency.iam.outputs.cluster_admin_role_arn
  runner_name                 = "${local.cluster_name}-gitlab-runner"
  terraform_state_bucket_name = local.terraform_state_bucket_name
  terraform_lock_table_name   = local.terraform_lock_table_name
  vpc_id                      = dependency.vpc.outputs.vpc_id
  vpc_subnet_ids              = local.use_private_subnets ? dependency.vpc.outputs.vpc_private_subnet_ids : dependency.vpc.outputs.vpc_public_subnet_ids
}
